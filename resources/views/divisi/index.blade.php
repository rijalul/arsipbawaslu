@extends('layouts.admin')

@section('content')

<div class="pt-3 pb-1" id="breadcrumbs-wrapper">
	<!-- Search for small screen-->
	<div class="container">
		<div class="row">
			<div class="col s12 m6 l6">
				<h5 class="breadcrumbs-title mt-0 mb-0"><span>Divisi</span></h5>
			</div>
		</div>
	</div>
</div>
<div class="col s12">
	<div class="container">
		<div class="section section-data-tables">
			<div class="card">
				<div class="card-content">
					<p class="caption mb-0">
						{{ Form::open(['route' => 'data.divisi.store','class'=>'formValidate0','id'=>'formValidate0']) }}
							<div class="row">
								<div class="input-field col s12">
									<label for="nama">Nama Divisi*</label>
									<input class="validate" required id="nama" name="nama" type="text">
								</div>
								<div class="input-field col s12">
									<button class="btn waves-effect waves-light right" type="submit" name="action">Submit
										<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
						{{ Form::close() }}
					</p>
					@if($errors->any())
					<div class="card-alert card red">
						<div class="card-content white-text">
							@foreach ($errors->all() as $error)
							<p>{{ $error }}</p>
							@endforeach
						</div>
					</div>
					@endif
					@if($message = Session::get('success'))
					<div class="card-alert card green">
						<div class="card-content white-text">
							<p>{{ $message }}</p>
						</div>
					</div>
					@endif
				</div>
			</div>
			<!-- Page Length Options -->
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content">
							<div class="row">
								<div class="col s12">
									<table id="page-length-option" class="striped Highlight">
										<thead>
											<tr>
												<th>No.</th>
												<th>Nama Divisi</th>
												<th>URL</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											@foreach($divisis as $d)
											<tr>
												<td>{{ ++$i }}</td>
												<td>{{ $d->nama }}</td>
												<td>{{ $d->slug }}</td>
												<td>button</td>
											</tr>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
												<th>No.</th>
												<th>Nama Divisi</th>
												<th>URL</th>
												<th></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	</div>
	<div class="content-overlay"></div>
</div>

@endsection

@push('styles')
	
@endpush

@push('scripts')
	<script src="{{ asset('js/form-validation.js') }}"></script>
@endpush