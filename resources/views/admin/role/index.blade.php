@extends('layouts.admin')

@section('content')

<div class="pt-3 pb-1" id="breadcrumbs-wrapper">
	<!-- Search for small screen-->
	<div class="container">
		<div class="row">
			<div class="col s12 m6 l6">
				<h5 class="breadcrumbs-title mt-0 mb-0"><span>Roles</span></h5>
			</div>
		</div>
	</div>
</div>
<div class="col s12">
	<div class="container">
		<div class="section section-data-tables">
			<!-- Page Length Options -->
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content">
							<div class="row">
								<div class="col s12">
									<table id="page-length-option" class="striped Highlight centered">
										<thead>
											<tr>
												<th>Rolename</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											@foreach($roles as $k)
											<tr>
												<td>{{ $k->nama }}</td>
											</tr>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
												<th>Rolename</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	</div>
	<div class="content-overlay"></div>
</div>

@endsection

@push('styles')
	
@endpush

@push('scripts')
@endpush