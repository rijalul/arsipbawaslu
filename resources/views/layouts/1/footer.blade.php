<!-- BEGIN: Footer-->
<footer class="page-footer footer footer-static footer-dark red gradient-shadow navbar-border navbar-shadow">
	<div class="footer-copyright">
		<div class="container"><span>&copy; 2020 <a>SIPBas</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="https://www.argiacyber.com/">ArgiaCyber</a></span></div>
	</div>
</footer>
<!-- END: Footer-->