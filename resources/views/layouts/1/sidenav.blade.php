<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark red sidenav-active-rounded">
	<div class="brand-sidebar" style="background: #f44336">
		<h1 class="logo-wrapper">
			<a class="brand-logo red" href="index.html">
				{{-- <img class="hide-on-med-and-down " src="../../../app-assets/images/logo/materialize-logo.png" alt="materialize logo" />
				<img class="show-on-medium-and-down hide-on-med-and-up" src="../../../app-assets/images/logo/materialize-logo-color.png" alt="materialize logo" /> --}}
				<span class="logo-text hide-on-med-and-down">Arsip Bawaslu</span>
			</a>
			{{-- <a class="navbar-toggler" href="#">
				<i class="material-icons">radio_button_checked</i>
			</a> --}}
		</h1>
	</div>
	<ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
		<li class="navigation-header">
			<a class="navigation-header-text" style="color: #ffffff">Data Arsip</a>
			<i class="navigation-header-icon material-icons">more_horiz</i>
		</li>
		<li class="{{ (request()->is('sipbas/divisi*') ? 'active bold' : '') }}">
			<a class="waves-effect waves-cyan {{ (request()->is('sipbas/divisi*') ? 'active' : '') }}" href="{{ route('data.divisi.index') }}">
				<i class="material-icons">business</i>
				<span class="menu-title" data-i18n="Divisi">Divisi</span>
			</a>
		</li>
		<li class="{{ (request()->is('sipbas/kategori*') ? 'active bold' : '') }}">
			<a class="waves-effect waves-cyan {{ (request()->is('sipbas/kategori*') ? 'active' : '') }}" href="{{ route('data.kategori.index') }}">
				<i class="material-icons">call_split</i>
				<span class="menu-title" data-i18n="Kategori">Kategori</span>
			</a>
		</li>
		<li class="{{ (request()->is('sipbas/arsip*') ? 'active bold' : '') }}">
			<a class="waves-effect waves-cyan {{ (request()->is('sipbas/arsip*') ? 'active' : '') }}" href="{{ route('data.arsip.index') }}">
				<i class="material-icons">book</i>
				<span class="menu-title" data-i18n="Arsip">Arsip</span>
			</a>
		</li>

		<li class="navigation-header">
			<a class="navigation-header-text" style="color: #ffffff">Data User</a>
			<i class="navigation-header-icon material-icons">more_horiz</i>
		</li>
		<li class="{{ (request()->is('private/users*') ? 'active bold' : '') }}">
			<a class="waves-effect waves-cyan {{ (request()->is('private/users*') ? 'active' : '') }}" href="{{ route('admin.users.index') }}">
				<i class="material-icons">face</i>
				<span class="menu-title" data-i18n="Divisi">Users</span>
			</a>
		</li>
		<li class="{{ (request()->is('private/roles*') ? 'active bold' : '') }}">
			<a class="waves-effect waves-cyan {{ (request()->is('private/roles*') ? 'active' : '') }}" href="{{ route('admin.roles.index') }}">
				<i class="material-icons">accessibility</i>
				<span class="menu-title" data-i18n="Divisi">Roles</span>
			</a>
		</li>
	</ul>
</aside>
<!-- END: SideNav-->