<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
	<div class="navbar navbar-fixed">
		<nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark red">
			<div class="nav-wrapper">
				<ul class="navbar-list right">
					<li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
					<li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="{{ asset('images/avatar-7.png') }}" alt="avatar"><i></i></span></a></li>
				</ul>
				<!-- profile-dropdown-->
				<ul class="dropdown-content" id="profile-dropdown">
					<li class="divider"></li>
					<li><a class="grey-text text-darken-1" href="user-login.html"><i class="material-icons">keyboard_tab</i> Logout</a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>
<!-- END: Header-->