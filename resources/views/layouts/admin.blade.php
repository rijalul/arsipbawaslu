<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/vendors.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/flag-icon.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/select.dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/materialize.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/data-tables.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
	@stack('styles')
</head>
<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-gradient-menu preload-transitions 2-columns" data-open="click" data-menu="vertical-gradient-menu" data-col="2-columns">
	@include('layouts.1.header')
	
	@include('layouts.1.sidenav')
	<div id="main">
        <div class="row">
			@yield('content')
		</div>
	</div>

	@include('layouts.1.footer')
	
	<script src="{{ asset('js/vendors.min.js') }}"></script>
	<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('js/dataTables.select.min.js') }}"></script>
	<script src="{{ asset('js/plugins.js') }}"></script>
	<script src="{{ asset('js/search.js') }}"></script>
	<script src="{{ asset('js/custom-script.js') }}"></script>
	<script src="{{ asset('js/data-tables.js') }}"></script>
	@stack('scripts')
</body>
</html>
