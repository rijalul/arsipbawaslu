@extends('layouts.admin')

@section('content')

<div class="pt-3 pb-1" id="breadcrumbs-wrapper">
	<!-- Search for small screen-->
	<div class="container">
		<div class="row">
			<div class="col s12 m6 l6">
				<h5 class="breadcrumbs-title mt-0 mb-0"><span>Data Arsip</span></h5>
			</div>
		</div>
	</div>
</div>
<div class="col s12">
	<div class="container">
		<div class="section section-data-tables">
			<div class="card">
				<div class="card-content">
					<p class="caption mb-0">
						{{ Form::open(['route' => 'data.arsip.store','files'=>true,'class'=>'formValidate0','id'=>'formValidate0']) }}
							<div class="row">
								<div class="input-field col s12">
									{{ Form::select('kategori_id', $kategoris, null, ['placeholder' => 'Pilih Kategori','class'=>'validate']) }}
								</div>
								<div class="input-field col s12">
									<textarea id="deskripsi" name="deskripsi" class="materialize-textarea validate" required>{{ old('deskripsi') }}</textarea>
									<label for="deskripsi">Deskripsi</label>
								</div>
								<div class="input-field col s12">
									{{ Form::select('divisi_id', $divisis, null, ['placeholder' => 'Pilih Divisi','class'=>'validate']) }}
								</div>
								<div class="input-field col s12">
									<input type="text" class="datepicker validate" id="tgl_terbit" name="tgl_terbit" value="{{ old('tgl_terbit') }}">
									<label for="tgl_terbit">Tgl Terbit/Terima</label>
								</div>
								<div class="col s12 file-field input-field">
									<div class="btn float-left">
										<span>Image Upload</span>
										<input type="file" name="image_upload">
									</div>
									<div class="file-path-wrapper">
										<input class="file-path validate" type="text" value="{{ old('image_upload') }}">
									</div>
								</div>
								<div class="col s12 file-field input-field">
									<div class="btn float-left">
										<span>File Upload</span>
										<input type="file" name="file_upload">
									</div>
									<div class="file-path-wrapper">
										<input class="file-path validate" type="text" value="{{ old('file_upload') }}">
									</div>
								</div>
								<div class="input-field col s12">
									<button class="btn waves-effect waves-light right" type="submit" name="action">Submit
										<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
						{{ Form::close() }}
					</p>
					@if($errors->any())
					<div class="card-alert card red">
						<div class="card-content white-text">
							@foreach ($errors->all() as $error)
							<p>{{ $error }}</p>
							@endforeach
						</div>
					</div>
					@endif
					@if($message = Session::get('success'))
					<div class="card-alert card green">
						<div class="card-content white-text">
							<p>{{ $message }}</p>
						</div>
					</div>
					@endif
				</div>
			</div>
			<!-- Page Length Options -->
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content">
							<div class="row">
								<div class="col s12">
									<table id="page-length-option" class="striped Highlight">
										<thead>
											<tr>
												<th width="3%">No.</th>
												<th width="5%">Kategori</th>
												<th width="5%">Deskripsi</th>
												<th width="5%">Divisi</th>
												<th width="3%">Terbit/Terima</th>
												<th width="5%">Publisher</th>
												<th width="5%"></th>
											</tr>
										</thead>
										<tbody>
											@php
											$i = 0;	
											@endphp
											@foreach($arsips as $a)
											<tr>
												<td>{{ ++$i }}</td>
												<td><span class="badge red">{{ $a->kategori->nama }}</span></td>
												<td>
													<a class="waves-effect waves-light btn modal-trigger" href="#modal{{ $a->id }}">Detail</a>
													<div id="modal{{ $a->id }}" class="modal bottom-sheet">
														<div class="modal-content">
															<h4>Deskripsi</h4>
															<p>{{ $a->deskripsi }}</p>
															<h4>Image Upload</h4>
															<p>{{ asset('asset/images/'.$a->image_upload) }}</p>
															<h4>File Upload</h4>
															<p>{{ asset('asset/file/'.$a->file_upload) }}</p>
														</div>
													</div>
												</td>
												<td><span class="badge blue">{{ $a->divisi->nama }}</span></td>
												<td>{{ $a->tgl_terbit }}</td>
												<td><span class="badge cyan">{{ $a->user->name }}</span></td>
												<td>
													<a class="mb-6 btn-floating waves-effect waves-light red accent-2">
														<i class="material-icons">clear</i>
													</a>
													<a class="mb-6 btn-floating waves-effect waves-light red accent-2">
														<i class="material-icons">clear</i>
													</a>
												</td>
											</tr>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
												<th>No.</th>
												<th>Kategori</th>
												<th>Deskripsi</th>
												<th>Divisi</th>
												<th>Tanggal Terbit/Terima</th>
												<th>Publisher/Editor</th>
												<th></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	</div>
	<div class="content-overlay"></div>
</div>

@endsection

@push('styles')
	
@endpush

@push('scripts')
	<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('.modal').modal();
			$('.datepicker').datepicker();
		});
	</script>
@endpush