<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::name('data.')->prefix('sipbas')->group(function(){
	Route::resource('divisi', 'DivisiController');
	Route::resource('kategori', 'KategoriController');
	Route::resource('arsip', 'ArsipController');
});

Route::name('admin.')->prefix('private')->group(function(){
	Route::resource('users','UsersController');
	Route::patch('users/{id}/update/role','UsersController@changerole')->name('users.role.change');
	Route::resource('roles','RoleController');
});