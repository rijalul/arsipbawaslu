<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
	protected $table 	= 'divisi';
	protected $fillable = ['nama','slug'];
	public function arsip()
	{
		return $this->hasMany('App\Arsip','divisi_id');
	}
}
