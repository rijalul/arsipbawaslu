<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
	protected $table 	= 'kategori';
	protected $fillable = ['nama','slug'];
	public function arsip()
	{
		return $this->hasMany('App\Arsip','kategori_id');
	}
}
