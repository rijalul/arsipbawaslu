<?php

namespace App\Http\Controllers;

use App\Divisi;
use Illuminate\Http\Request;

class DivisiController extends Controller
{

	public function index()
	{
		$divisis = Divisi::latest()->paginate(10);
		return view('divisi.index',compact('divisis'))
		->with('i', (request()->input('page', 1) - 1) * 10);
	}

	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		$request->validate([
            'nama' => 'required|unique:divisi'
		]);
		
		$data = new Divisi;
		$data->nama = $request->nama;
		$data->slug = str_slug($request->nama);
		$data->save();

		return redirect()->back()->with('success','Data'.$request->nama.'Berhasil Diinputkan.');
	}

	public function show(Divisi $divisi)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Divisi  $divisi
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Divisi $divisi)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Divisi  $divisi
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Divisi $divisi)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Divisi  $divisi
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Divisi $divisi)
	{
		//
	}
}
