<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{

	public function index()
	{
		$kategoris = Kategori::latest()->paginate(10);
		return view('kategori.index',compact('kategoris'))
		->with('i', (request()->input('page', 1) - 1) * 10);
	}

	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		$request->validate([
			'nama' => 'required|unique:kategori'
		]);
		$data = new Kategori;
		$data->nama = $request->nama;
		$data->slug = str_slug($request->nama);
		$data->save();
		return redirect()->back()->with('success','Data '.$request->nama.' Berhasil Diinputkan.');
	}

	public function show(Kategori $kategori)
	{
		//
	}

	public function edit(Kategori $kategori)
	{
		//
	}

	public function update(Request $request, Kategori $kategori)
	{
		//
	}

	public function destroy(Kategori $kategori)
	{
		//
	}
}
