<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;

use Illuminate\Http\Request;

class UsersController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$users 	= User::with('role')->orderBy('id','desc')->get();
		$roles 	= Role::pluck('nama','id');
		return view('admin.user.index', compact('users','roles'));
	}

	public function create()
	{
		//
	}

	public function store(Request $request)
	{
		//
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		//
	}

	public function update(Request $request, $id)
	{
		//
	}

	public function changerole(Request $request, $id)
	{
		// dd($request->all());
		$user = User::where('id','=',$id)->first();
		$user->role_id = $request->role_id;
		$user->save();

		return redirect()->back()->with('success','Update Role Berhasil.');
	}

	public function destroy($id)
	{
		//
	}
}
