<?php

namespace App\Http\Controllers;

use App\Arsip;
use App\Kategori;
use App\Divisi;
use Illuminate\Http\Request;

class ArsipController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		
		$kategoris 	= Kategori::pluck('nama','id');
		$divisis 	= Divisi::pluck('nama','id');
		$arsips 	= Arsip::with('kategori')->orderBy('id','desc')->get();
		// dd($arsips);
		return view('arsip.index',compact('arsips','kategoris','divisis'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// dd($request->all());
		$request->validate([
			'kategori_id' 	=> 'required',
			'deskripsi' 	=> 'required',
			'divisi_id' 	=> 'required',
			'tgl_terbit'	=> 'required',
			'file_upload'	=> 'required|mimes:pdf,docs,docx|max:2048',
			'image_upload' 	=> 'required|image|mimes:jpeg,png,jpg|max:2048',
		]);
		// image upload
		$image = $request->file('image_upload');
		$newimage_name = 'image_' . date("M-D-Y") . '_' . str_slug($image->getClientOriginalName()) . '.' .$image->getClientOriginalExtension();
		$image->move(public_path('asset/images'), $newimage_name);
		// image upload
		// file upload
		$file_u = $request->file('file_upload');
		$newfile_name = 'image_' . date("M-D-Y") . '_' . str_slug($file_u->getClientOriginalName()) . '.' .$file_u->getClientOriginalExtension();
		$file_u->move(public_path('asset/file'), $newfile_name);
		// file upload

		$form_data = array(
            'kategori_id' 	=> $request->kategori_id,
			'deskripsi' 	=> $request->deskripsi,
			'divisi_id' 	=> $request->divisi_id,
			'tgl_terbit'	=> $request->tgl_terbit,
			'file_upload'	=> $newfile_name,
			'image_upload' 	=> $newimage_name,
			'user_id'		=> auth()->user()->id,
		);
		Arsip::create($form_data);
		return redirect()->back()->with('success','Data Arsip Berhasil Diinputkan');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Arsip  $arsip
	 * @return \Illuminate\Http\Response
	 */
	public function show(Arsip $arsip)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Arsip  $arsip
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Arsip $arsip)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Arsip  $arsip
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Arsip $arsip)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Arsip  $arsip
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Arsip $arsip)
	{
		//
	}
}
