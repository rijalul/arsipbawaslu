<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arsip extends Model
{
	protected $table    = 'arsip';
	protected $fillable = ['kategori_id','deskripsi','divisi_id','tgl_terbit','file_upload','image_upload','user_id'];
	public function kategori()
	{
		return $this->belongsTo('App\Kategori','id');
	}
	public function divisi()
	{
		return $this->belongsTo('App\Divisi','id');
	}
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
