<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
			[
                'nama'              => 'Super Admin',
                'created_at'        => Carbon::now(),
				'updated_at'        => Carbon::now(),
			],
			[
				'nama'              => 'Admin',
				'created_at'        => Carbon::now(),
				'updated_at'        => Carbon::now(),
            ],
            [
				'nama'              => 'User',
				'created_at'        => Carbon::now(),
				'updated_at'        => Carbon::now(),
            ],
            [
				'nama'              => 'User Belum Terverifikasi',
				'created_at'        => Carbon::now(),
				'updated_at'        => Carbon::now(),
            ],
		]);
    }
}
