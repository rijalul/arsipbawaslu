<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class KategoriesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('kategori')->insert([
			[
				'nama'			=> 'Surat Masuk',
				'slug'			=> 'surat-masuk',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Surat Keluar',
				'slug'			=> 'surat-keluar',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Form A Pengawasan',
				'slug'			=> 'form-a-pengawasan',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Hasil Pengawasan',
				'slug'			=> 'hasil-pengawasan',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Form A Penanganan Pelanggaran',
				'slug'			=> 'form-a-penanganan-pelanggaran',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Keputusan',
				'slug'			=> 'keputusan',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Imbauan',
				'slug'			=> 'imbauan',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Rekomendasi',
				'slug'			=> 'rekomendasi',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Penyelesaian Sengketa',
				'slug'			=> 'penyelesaian-sengketa',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Laporan',
				'slug'			=> 'laporan',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			[
				'nama'			=> 'Temuan',
				'slug'			=> 'temuan',
				'created_at'	=> Carbon::now(),
				'updated_at'	=> Carbon::now(),
			],
			
		]);
	}
}
