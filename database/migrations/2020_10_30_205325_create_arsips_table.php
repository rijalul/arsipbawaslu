<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArsipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arsip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kategori_id')->nulled();
            $table->text('deskripsi')->nulled();
            $table->integer('divisi_id')->nulled();
            $table->string('tgl_terbit')->nulled();
            $table->text('file_upload')->nulled();
            $table->text('image_upload')->nulled();
            $table->bigInteger('user_id')->nulled();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arsip');
    }
}
